Snoopy The Observer
==================

Pre-requisites
--------------

To run this project you will need following items
installed on your machine:

   - [Python Poetry](https://python-poetry.org/)

This project has been developed and tested on **Python 3.9 on Linux**

Documentation
-------------

Full documentation is available on [dedicated GitLab Page](https://nemesius.gitlab.io/snoopy/).


Usage and installation
-------------

To install:

1. Navigate to application folder
2. Run `poetry shell`
3. Run `poetry install`
4. Run `python snoopy/cli.py` with required parameters

```
Options:
  -p, --process TEXT            Name of the process to monitor  [required]
  -d, --duration INTEGER RANGE  Monitoring duration in seconds  [required]
  -i, --interval INTEGER RANGE  Sampling frequency in seconds. Should not be
                                bigger than duration  [default: 5]

  -ep, --export_path PATH       Custom path for results export  [default: .]
  --help                        Show this message and exit.
```

Example usage:
```
python3 snoopy/cli.py -p firefox -d 5 -i 1
```

Running tests
-------------

Tests can be run 2 ways, both from application folder:

**pytest**

    pytest --cov


**nox**

    nox -s tests