App concept
==================

.. contents::
    :local:
    :backlinks: none

Simplified architecture
-----------------------
.. figure:: images/snoopy_concept.svg

App requirements
----------------
#. Periodically gathers process metrics (for a specified amount of time)
#. Creates a report of the gathered process metrics (in CSV format)
#. Outputs the average for each process metric

The metrics that should be gathered for the process are:

#. % of CPU used
#. Private memory used
#. Number of open handles / file descriptors

App input restrictions
----------------------
#. The process name (mandatory)
#. The overall duration of the monitoring in seconds (mandatory)
#. The sampling interval in seconds (optional, by default 5 sec if not specified)

Design
------
Aplication consists of: 

#. CLI module that provides user interface, handles start of the main routine and validates user input
#. Monitoring module that handles data processing and export
#. Probing module that contains interface for probing tools and their implementations

Data export consideration
~~~~~~~~~~~~~~~~~~~~~~~~~
Data is being exported into CSV file. Export naming convention is:

.. code-block:: console

  <process_name>_monitoring_results_<current_date>.csv

Each export is enriched with unique timestamp. Additionally if file already exists, new data will append instead of overwrite
