"""Sphinx configuration."""
project = "snoopy"
author = "Jakub Rybacki"
copyright = f"2021, {author}"
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_autodoc_typehints",
]
