Snoopy The Observer
===================

.. toctree::
   :hidden:
   :maxdepth: 1

   concept
   test_strategy
   reference


Pre-requisites
--------------

To run this project you will need following items
installed on your machine:

    * Python Poetry <https://python-poetry.org/>


This project has been developed and tested on **Python 3.9 on Linux** 

Usage and installation
----------------------

To install:

#. Navigate to application folder
#. Run `poetry shell`
#. Run `poetry install`
#. Run `python snoopy/cli.py` with required parameters

.. code-block:: console

    Options:
    -p, --process TEXT            Name of the process to monitor  [required]
    -d, --duration INTEGER RANGE  Monitoring duration in seconds  [required]
    -i, --interval INTEGER RANGE  Sampling frequency in seconds. Should not be
                                    bigger than duration  [default: 5]

    -ep, --export_path PATH       Custom path for results export  [default: .]
    --help                        Show this message and exit.

Running tests
-------------

Tests can be run 2 ways:

Using pytest
~~~~~~~~~~~~

While in application folder run 

.. code-block:: console

    pytest --cov

Using nox
~~~~~~~~~

.. code-block:: console

    nox -s tests
