Testing strategy
================

.. contents::
    :local:
    :backlinks: none

General approach
----------------
Regardless of the implementation, CLI or with UI, classical test pyramid is a tried and true way of layering tests. In case of this SUT specifically, Unit Tests play crucial role due to low level functions being crucial for proper operation. 

Agile approach to Test Plan
---------------------------
Concept of Test Plan is not the greatest match for Agile development. Instead we should focus on general strategies that should be implemented to ensure quality throughout the sprints. Below we can see one visualisation of testing quadrants that can help us categorise tests.

.. figure:: images/agile-test-quadrants.svg

As SUT is a monitoring tool main focus for testing should be on low level functions. This can be achieved by engaging developers in test writing as part of the process. We can achive this for example with GitOps by ensuring pre-commit hooks with test coverage goals and/or linters. Additionally, developers can start working in TDD which makes creating tests a pre-requisite.

Integration tests would cover interaction between data consumers (since application is required to enable export) as well as different probes. Below there is a visualisation of such test in form of sequence diagram

.. figure:: images/test_flow.svg

If we follow above flow and instead of mock we attach real implementation, we are running e2e test. This can be achieved by manipulating configuration and tied to specific triggers/environments.

Challenges and what tests to run
--------------------------------
There are multiple challenges linked with non-deterministic nature of the SUT

Basic checks
~~~~~~~~~~~~
Basic checks will include but not limit to:
  
#. Verifying input validation
#. Verifying exception handling
#. Checking of process discovery
#. Proper export of data

CPU metrics
~~~~~~~~~~~
Without knowing the secification we can only test in extremes. Either check for processes that are asleep or load them and see if our SUT shows proper usage. 

Multiple processes
~~~~~~~~~~~~~~~~~~

Another case we could use is to check how SUT reacts to multiple instances of the same process for example opening multiple browser windows or sessions.

Memory leaks
~~~~~~~~~~~~
One way to check for memory leaks is to note USS used by a process and see if it's being released after process is terminated. Since SUT does not monitor such values we could test it by running memory intensive process to see if it picks up increasing memory. These could include rendering, working with complex spreadsheets, compiling, modeling of any kind etc. If SUT can pickup memory deltas it might be able to detect runaway process too.