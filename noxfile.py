"""Nox sessions."""
import nox
from nox.sessions import Session
import nox_poetry

nox.options.sessions = "lint", "tests"
locations = ["snoopy", "tests", "noxfile.py", "docs/conf.py"]


@nox.session(python=["3.9"])
def tests(session: Session) -> None:
    """Runs tests."""
    args = session.posargs or ["--cov", "-n", "4"]
    nox_poetry.installroot(session, distribution_format=nox_poetry.WHEEL)
    session.run("poetry", "install", "--no-dev", external=True)
    nox_poetry.install(
        session,
        "coverage[toml]",
        "pytest",
        "pytest-cov",
        "pytest-xdist",
    )
    session.run("pytest", *args)


@nox.session(python=["3.9"])
def lint(session: Session) -> None:
    """ Runs linting checks """
    args = session.posargs or locations
    nox_poetry.installroot(session, distribution_format=nox_poetry.WHEEL)
    nox_poetry.install(
        session,
        "flake8",
        "flake8-annotations",
        "flake8-black",
        "flake8-bugbear",
        "flake8-import-order",
    )
    session.run("flake8", *args)


@nox.session(python=["3.9"])
def black(session: Session) -> None:
    """Runs formatting."""
    args = session.posargs or locations
    nox_poetry.installroot(session, distribution_format=nox_poetry.WHEEL)
    nox_poetry.install(session, "black")
    session.run("black", *args)


@nox.session(python=["3.9"])
def docs(session: Session) -> None:
    """Build the documentation."""
    nox_poetry.installroot(session, distribution_format=nox_poetry.WHEEL)
    session.run("poetry", "install", "--no-dev", external=True)
    nox_poetry.install(session, "sphinx", "sphinx-autodoc-typehints", "pytest")
    session.run("sphinx-build", "docs", "docs/_build")
