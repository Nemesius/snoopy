import sys

import click

from snoopy.monitors import LinuxMonitor
from snoopy.probes import PsutilProbe


@click.command()
@click.option(
    "-p",
    "--process",
    required=True,
    type=click.STRING,
    help="Name of the process to monitor",
)
@click.option(
    "-d",
    "--duration",
    required=True,
    type=click.IntRange(1, None),
    help="Monitoring duration in seconds",
)
@click.option(
    "-i",
    "--interval",
    default=5,
    type=click.IntRange(1, None),
    help="Sampling frequency in seconds. Should not be bigger than duration",
    show_default=True,
)
@click.option(
    "-ep",
    "--export_path",
    default=".",
    help="Custom path for results export",
    type=click.Path(exists=True, writable=True),
    show_default=True,
)
def cli(process: str, duration: int, interval: int, export_path: str) -> None:
    """
    Performs validation of input parameters and initiates monitoring process

    Args:
        process (str): Name of the process to monitor
        duration (int): Duration of monitoring
        interval (int): Interval between snapshots
        export_path (str): Custom path for export file

    Raises:
        click.BadParameter: Raised when duration < interval
    """
    if interval > duration:
        raise click.BadParameter(
            "Interval cannot by longer than duration of monitoring",
            param_hint=["--interval"],
        )

    click.echo(f"MONITORING STARTED FOR PROCESS: {process}")
    click.echo(
        f"PLATFORM:{sys.platform}\nDURATION:{duration}\nSAMPLING FREQUENCY:{interval}"
    )
    linux_snooper = LinuxMonitor(
        duration=duration,
        interval=interval,
        system_probe=PsutilProbe(),
        export_path=export_path,
    )
    linux_snooper.monitor_process(process_name=process)


if __name__ == "__main__":
    """
    Run CLI interface
    """
    cli()
