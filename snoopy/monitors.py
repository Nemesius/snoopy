from datetime import datetime
import os
from pathlib import Path
import threading
import time
from typing import Any, Dict, List

import pandas as pd

from snoopy.probes import ProcessProbe


class LinuxMonitor:
    """
    Monitoring class for handling Linux process' data gathering
    """

    def __init__(
        self, duration: int, interval: int, system_probe: ProcessProbe, export_path: str
    ):
        self.duration = duration
        self.interval = interval
        self.max_iterations = int(duration / interval)
        self.probe = system_probe
        self.export_path = export_path

    def extract_to_csv(self, df: pd.DataFrame, process: str) -> None:
        """
        Exports dataframe to file, either existing or new

        Args:
            df (DataFrame): Dataframe to export
        """
        current_date = datetime.today().strftime("%Y-%m-%d")
        file_name = f"{process}_monitoring_results_{current_date}.csv"
        file_path = Path(self.export_path)

        # Export to csv file
        destination = file_path / file_name
        if os.path.isfile(destination):
            df.to_csv(destination, index=False, mode="a", header=False)
        else:
            df.to_csv(destination, index=False, mode="w", header=True)

    def gather_process_data(self, process_name: str) -> List[Dict[str, Any]]:
        """
        Gathers system data for specifiec process name.
        It will take maximum amount of snapshots permitted by
        duration and interval

        Args:
            process_name (str): Name of the process to monitor

        Returns:
            List[Dict[str, Any]]: List with snapshots for all matching processes
        """
        data_gathered = list()
        num_samples = 0
        ticker = threading.Event()

        while not ticker.wait(self.interval) and num_samples < self.max_iterations:
            df_sample = pd.DataFrame.from_dict(
                self.probe.probe_system(process_name=process_name)
            )
            # TODO add memory leak warnings here if anything
            data_gathered.append(df_sample)
            num_samples += 1

        return data_gathered

    def transform_df(self, input_df: pd.DataFrame) -> pd.DataFrame:
        """
        Transforms dataframe by:
        - grouping by PID
        - calculating mean
        - setting precision
        - renaming columns
        - attaching timestamp of export

        Args:
            input_df (pd.DataFrame): Gathered process info

        Returns:
            pd.DataFrame: Transfromed dataframe
        """
        df_grouped = input_df.groupby("pid")
        df_means = df_grouped.mean().round(
            {"cpu_utility[%]": 2, "private_memory[KB]": 3}
        )

        df_renamed = df_means.rename(
            columns={
                "cpu_utility[%]": "mean_cpu_utility[%]",
                "private_memory[KB]": "mean_private_memory[KB]",
                "file_descriptors_open[count]": "mean_file_descriptors_open[count]",
            }
        )

        df_renamed["export_timestamp"] = int(time.mktime(datetime.today().timetuple()))

        return df_renamed

    def monitor_process(self, process_name: str):
        """
        Orchestrates monitoring of the process.
        Handles data gathering, concatenation, processing and export

        Args:
            process_name (str): Name of the process to monitor
        """
        # Get process data
        raw_process_data = self.gather_process_data(process_name=process_name)
        df_process_data = pd.concat(raw_process_data)

        # Transform data
        df_transformed = self.transform_df(df_process_data)

        # Extract to csv
        self.extract_to_csv(df=df_transformed, process=process_name)
