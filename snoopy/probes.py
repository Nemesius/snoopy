import sys
from typing import Any, Dict, List

from loguru import logger
from psutil import (
    AccessDenied,
    cpu_count,
    NoSuchProcess,
    Process,
    process_iter,
)


class ProcessProbe:
    """
    Process probe interface
    """

    def probe_system(self, process_name: str):
        raise NotImplementedError


class PsutilProbe(ProcessProbe):
    def find_processes_by_name(self, process_name: str) -> List[int]:
        """
        Finds PIDs of processes matching name criteria

        Args:
            process_name (str): Name of the process

        Returns:
            List[int]: List of pids found. Returns empty if no process matches criteria
        """
        pids_found = list()
        for proc in process_iter():
            proc_info = proc.as_dict(attrs=["pid", "name"])

            if proc_info["name"].lower() == process_name.lower():
                pids_found.append(proc_info["pid"])
                logger.success(
                    f"Process {process_name} found under PID: {proc_info['pid']}"
                )

        return pids_found

    def get_cpu_usage(self, pid: int) -> float:
        """
        Returns total CPU usage, disregarding individual core load

        Args:
            pid (int): PID of process to analyze

        Returns:
            float: Total CPU usage percentage
        """
        cpu_cores = cpu_count(logical=True)

        try:
            proc = Process(pid)
        except (NoSuchProcess, AccessDenied) as exp:
            raise exp

        proc.cpu_percent(interval=0)
        usage = proc.cpu_percent(interval=0.1)

        return usage / cpu_cores

    def get_private_memory(self, pid: int) -> int:
        """
        Returns private memory of a process (USS), in KB.
        Unique Set Size represents libraries and pages
        allocated only to this process.

        Args:
            pid (int): PID of process to analyze

        Returns:
            int: USS memory [KB]
        """
        try:
            proc = Process(pid)
        except (NoSuchProcess, AccessDenied) as exp:
            raise exp

        with proc.oneshot():
            return proc.memory_full_info().uss / 1e3

    def get_file_descriptors(self, pid: int) -> int:
        """
        Return file descriptors attached to a process

        Args:
            pid (int): PID of process

        Returns:
            int: Number of file descriptors opened
        """
        try:
            proc = Process(pid)
        except (NoSuchProcess, AccessDenied) as exp:
            raise exp

        with proc.oneshot():
            return proc.num_fds()

    def get_process_stats(self, pid: int) -> Dict[str, Any]:
        """
        Returns set of information about process resource usage

        Args:
            pid (int): PID of the process

        Returns:
            Dict[str, Any]: Dict with pid and snapshot results
        """
        return {
            "pid": pid,
            "cpu_utility[%]": self.get_cpu_usage(pid=pid),
            "private_memory[KB]": self.get_private_memory(pid=pid),
            "file_descriptors_open[count]": self.get_file_descriptors(pid=pid),
        }

    def probe_system(self, process_name: str) -> List[Dict[str, Any]]:
        """
        Searches for processes matching criteria and returns their stat snapshots

        Args:
            process_name (str): Name of the process to find

        Returns:
            List[Dict[str, Any]]: Stats' snapshots of all processes matching criteria
        """
        pids = self.find_processes_by_name(process_name=process_name)

        if not pids:
            sys.exit(f"No processes found named: {process_name}")

        results = list()

        for pid in pids:
            results.append(self.get_process_stats(pid))

        return results
