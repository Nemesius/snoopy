import pytest

from snoopy.monitors import LinuxMonitor
from snoopy.probes import ProcessProbe, PsutilProbe


####################
#      STUBS       #
####################


@pytest.fixture
def probe_stub():
    """
    Implements a probe stub

    Returns:
        FakeProbe: ProcessProbe stub
    """

    class FakeProbe(ProcessProbe):
        def probe_system(self, process_name: str):

            return [
                {
                    "pid": 1234,
                    "cpu_utility[%]": 12.5,
                    "private_memory[KB]": 567.450,
                    "file_descriptors_open[count]": 15,
                },
                {
                    "pid": 5678,
                    "cpu_utility[%]": 4,
                    "private_memory[KB]": 0.550,
                    "file_descriptors_open[count]": 20,
                },
                {
                    "pid": 9012,
                    "cpu_utility[%]": 2,
                    "private_memory[KB]": 133,
                    "file_descriptors_open[count]": 1,
                },
                {
                    "pid": 3456,
                    "cpu_utility[%]": 1.5,
                    "private_memory[KB]": 560000.000,
                    "file_descriptors_open[count]": 135,
                },
            ]

    fake_probe = FakeProbe()
    return fake_probe


@pytest.fixture
def psutil_test_probe() -> PsutilProbe:
    """
    Creates new instance of PsutilProbe

    Returns:
        Psutil probe: Probe instance
    """

    p = PsutilProbe()
    return p


@pytest.fixture
def linux_test_monitor(probe_stub, test_export_dir) -> LinuxMonitor:
    """
    Creates test version of LinuxMonitor with:
    - duration: 1
    - interval: 1
    - probe: tubbed by fixture probe_stub
    - export_path: supplemented by pytest test folders

    Returns:
        LinuxMonitor: LinuxMonitor with test parameters
    """
    lm = LinuxMonitor(
        duration=1, interval=1, system_probe=probe_stub, export_path=test_export_dir
    )
    return lm


####################
# HELPER FUNCTIONS #
####################


@pytest.fixture(scope="session")
def test_export_dir(tmpdir_factory) -> str:
    """
    Creates temporary directory used for storing test exports

    Args:
        tmpdir_factory: Pytest fixture

    Returns:
        str: Path to temporary folder
    """
    export_tmpdir = tmpdir_factory.mktemp("test_export")
    return export_tmpdir
