from datetime import datetime
import time

import pandas as pd
from pandas.testing import assert_frame_equal
import pytest


class TestLinuxMonitor:
    def test_should_gather_process_data(self, linux_test_monitor):
        # Arrange
        expected_df = pd.DataFrame().from_dict(
            {
                "pid": [1234, 5678, 9012, 3456],
                "cpu_utility[%]": [12.5, 4, 2, 1.5],
                "private_memory[KB]": [567.45, 0.55, 133, 560000],
                "file_descriptors_open[count]": [15, 20, 1, 135],
            }
        )

        # Act
        results = linux_test_monitor.gather_process_data(process_name="test")

        # Assert
        assert_frame_equal(results[0], expected_df)

    df_transform = [
        (
            pd.DataFrame(
                {
                    "pid": [1234, 5678, 9012, 3456],
                    "cpu_utility[%]": [12.5111111, 4.123, 245.12567, 1.5],
                    "private_memory[KB]": [567.45346, 0.55111, 133, 560000],
                    "file_descriptors_open[count]": [15, 20, 1, 135],
                }
            ),
            pd.DataFrame(
                {
                    "mean_cpu_utility[%]": [12.51, 1.5, 4.12, 245.13],
                    "mean_private_memory[KB]": [567.453, 560000.0, 0.551, 133.0],
                    "mean_file_descriptors_open[count]": [15, 135, 20, 1],
                },
                index=[1234, 3456, 5678, 9012],
            ),
        )
    ]

    @pytest.mark.parametrize("input_df, expected", df_transform)
    def test_should_transform_df(self, input_df, expected, linux_test_monitor):
        # Arrange
        expected.index.rename("pid", inplace=True)
        expected["export_timestamp"] = int(time.mktime(datetime.today().timetuple()))

        # Act
        result = linux_test_monitor.transform_df(input_df=input_df)

        # Assert
        assert_frame_equal(result, expected)

    df_export = [
        pd.DataFrame(
            {
                "mean_cpu_utility[%]": [12.5, 1.5, 4, 2],
                "mean_private_memory[KB]": [567.45, 560000.0, 0.55, 133.0],
                "mean_file_descriptors_open[count]": [15, 135, 20, 1],
            },
            index=[0, 1, 2, 3],
        ),
    ]

    @pytest.mark.parametrize("expected", df_export)
    def test_should_export_gathered_data(self, linux_test_monitor, expected):
        # Arrange
        test_process_name = "test_process"
        current_date = datetime.today().strftime("%Y-%m-%d")
        test_file_name = f"{test_process_name}_monitoring_results_{current_date}.csv"
        expected["export_timestamp"] = int(time.mktime(datetime.today().timetuple()))

        # Act
        linux_test_monitor.monitor_process(process_name=test_process_name)

        exported_df = pd.read_csv(f"{linux_test_monitor.export_path}/{test_file_name}")

        # Assert
        assert_frame_equal(exported_df, expected)
