from psutil import NoSuchProcess, pids
import pytest


def assert_metric_validity(result):
    # System metrics are non-deterministic therefore
    # only way to test the probes is to check
    # measurement validity
    assert result >= 0


class TestPsutilProbe:
    @pytest.mark.parametrize("test_input", [("python")])
    def test_should_find_a_process(self, test_input, psutil_test_probe):
        # Act
        result = psutil_test_probe.find_processes_by_name(process_name=test_input)

        # Assert
        assert result != []

    @pytest.mark.negative
    def test_should_return_empty_if_process_not_found(self, psutil_test_probe):
        # Act
        result = psutil_test_probe.find_processes_by_name(
            process_name="testing_process"
        )

        # Assert
        assert result == []

    @pytest.mark.exception
    def test_exception_raised_when_no_process_for_cpu_probe(self, psutil_test_probe):
        with pytest.raises(NoSuchProcess):
            # Act
            psutil_test_probe.get_cpu_usage(pid=0)

    @pytest.mark.exception
    def test_exception_raised_when_no_process_for_mem_probe(self, psutil_test_probe):
        with pytest.raises(NoSuchProcess):
            # Act
            psutil_test_probe.get_private_memory(pid=0)

    @pytest.mark.exception
    def test_exception_raised_when_no_process_for_fd_probe(self, psutil_test_probe):
        with pytest.raises(NoSuchProcess):
            # Act
            psutil_test_probe.get_file_descriptors(pid=0)

    def test_should_return_proper_reading_from_cpu(self, psutil_test_probe):
        # Arrange
        pid_list = pids()

        # Act
        result = psutil_test_probe.get_cpu_usage(pid=pid_list[-1])

        # Assert
        assert_metric_validity(result)

    def test_should_return_proper_reading_memory(self, psutil_test_probe):
        # Arrange
        pid_list = pids()

        # Act
        result = psutil_test_probe.get_private_memory(pid=pid_list[-1])

        # Assert
        assert_metric_validity(result)

    def test_should_return_proper_reading_file_descriptors(self, psutil_test_probe):
        # Arrange
        pid_list = pids()

        # Act
        result = psutil_test_probe.get_file_descriptors(pid=pid_list[-1])

        # Assert
        assert_metric_validity(result)

    def test_should_return_snapshot_of_metrics(self, psutil_test_probe):
        # Arrange
        pid_list = pids()
        expected_keys = [
            "pid",
            "cpu_utility[%]",
            "private_memory[KB]",
            "file_descriptors_open[count]",
        ]

        # Act
        result = psutil_test_probe.get_process_stats(pid=pid_list[-1])

        # Assert
        output_keys = result.keys()
        assert result["pid"] == pid_list[-1]
        for key in expected_keys:
            assert key in output_keys

    def test_should_return_list_of_snapshots(self, psutil_test_probe):
        # Act
        result = psutil_test_probe.probe_system(process_name="python")

        # Assert
        assert len(result) > 0

    @pytest.mark.exception
    @pytest.mark.parametrize("test_input", [("test_process")])
    def test_should_stop_probing_when_no_process_found(
        self, psutil_test_probe, test_input
    ):
        with pytest.raises(SystemExit) as se:
            # Act
            psutil_test_probe.probe_system(process_name=test_input)

            # Assert
            assert se.msg == f"No processes found named: {test_input}"
